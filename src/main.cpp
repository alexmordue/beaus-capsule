#include <Arduino.h>
#include "DNSServer.h"                  // Patched lib
#include "ESPTemplateProcessor.h"
#include <ESP8266WebServer.h>
#include <NeoPixelBus.h>
#include <EEPROM.h>

#define COLOUR_LENGTH 4
#define NUM_COLOURS 1
#define EEPROM_CONFIG_LENGTH (COLOUR_LENGTH * NUM_COLOURS)

const uint16_t PixelCount = 5;
const uint8_t PixelPin = 4;  // make sure to set this to the correct pin, ignored for Esp8266

RgbwColor black(0);
RgbwColor white(255);
RgbwColor left(255,0,0,0);
RgbwColor right(0,0,255,0);
float sweep=0.0;
boolean sweepGoingUp=true;
int ledNumber=0;
boolean cleaningLeds=false;

RgbwColor currentColor(0);

NeoGamma<NeoGammaTableMethod> colorGamma; // for any fade animations, best to correct gamma

NeoPixelBus<NeoGrbwFeature, NeoEsp8266Dma800KbpsMethod > strip(PixelCount, PixelPin);

const byte        DNS_PORT = 53;          // Capture DNS requests on port 53
IPAddress         apIP(10, 10, 10, 1);    // Private network for server
DNSServer         dnsServer;              // Create the DNS object
ESP8266WebServer  webServer(80);          // HTTP server

bool alert=false;

bool topLightOnly=false;

void loadColour(uint8_t * colourArray, uint8_t index){
  if (index>=NUM_COLOURS){
    // we don't have anything sotred for this index, return black
    for(int i=0;i<COLOUR_LENGTH;i++){
      colourArray[i]=0;
    }
    return;
  }
  int offset = COLOUR_LENGTH * index;
  for(int i=0;i<COLOUR_LENGTH;i++){
    colourArray[i]=EEPROM.read(offset+i);
  }
}

void saveColour(uint8_t red, uint8_t green, uint8_t blue, uint8_t white, uint8_t index){
  Serial.println("Saving colour index: "+ String(index, DEC) + " with colour " + String(red, HEX)+ String(green, HEX)+ String(blue, HEX)+ String(white, HEX));
  if (index>=NUM_COLOURS){
    return;
  }
  int offset = COLOUR_LENGTH * index;
  EEPROM.write(offset, red);
  offset++;
  EEPROM.write(offset, green);
  offset++;
  EEPROM.write(offset, blue);
  offset++;
  EEPROM.write(offset, white);
  offset++;
  EEPROM.commit();
}

void setColourFromPreset(uint8_t index){
  uint8_t presetColourArray[COLOUR_LENGTH];
  loadColour(presetColourArray, index);
  RgbwColor presetColour = RgbwColor(presetColourArray[0], presetColourArray[1], presetColourArray[2], presetColourArray[3]);
  if(!topLightOnly){
    strip.ClearTo(presetColour);
  }else{
    strip.ClearTo(black);
    strip.SetPixelColor(PixelCount-1,presetColour );
  }
  strip.Show();
}

bool hexStringToByteArray(String hexString, uint8_t * theArray, int expectedLength){
  if(hexString.length()<expectedLength){return false;}
  hexString.toLowerCase();
  Serial.println("Turning string into byteArray: " + hexString);
  char currentChar = '0';
  bool highnibble = false;
  char nibbleValue = 0;
  uint8_t currentByte = 0;
  Serial.println("Converting "+String(expectedLength)+" chars");
  for(int i=0;i<expectedLength;i++){
    Serial.println("Processing Char Number: " + String(i,DEC));
    if(i%2 == 0){highnibble=true;}else{highnibble=false;}
    currentChar = hexString.charAt(i);
    Serial.println("Char: " + String(currentChar));
    nibbleValue = currentChar-'0';
    if(nibbleValue>9){
      nibbleValue = 10+(currentChar-'a');
    }
    Serial.println("Nibble: " + String(nibbleValue, HEX));
    if (highnibble){
      currentByte = 0;
      currentByte = nibbleValue << 4;
    }else{
      currentByte = currentByte | nibbleValue;
      theArray[i/2] = currentByte;
      Serial.println("Byte: " + String(currentByte, HEX));
    }
  }
  Serial.println("hexStringToByteArray Done");
}

String indexProcessor(const String& key) {
  Serial.println(String("KEY IS ") + key);
  if (key == "TITLE") return "Hello World!";
  else if (key == "BODY") return "It works!";
  else if (key == "") return "@";

  return "oops";
}

String colourPickerProcessor(const String& key) {
  uint8_t presetColourArray[COLOUR_LENGTH];
  loadColour(presetColourArray, 0);
  Serial.println(String("KEY IS ") + key);
  if (key == "RED") return String(presetColourArray[0], DEC);
  else if (key == "GREEN") return String(presetColourArray[1], DEC);
  else if (key == "BLUE") return String(presetColourArray[2], DEC);
  else if (key == "WHITE") return String(presetColourArray[3], DEC);
  else if (key == "") return "@";

  return "0";
}

String getContentType(String filename){
  if(webServer.hasArg("download")) return "application/octet-stream";
  else if(filename.endsWith(".htm")) return "text/html";
  else if(filename.endsWith(".html")) return "text/html";
  else if(filename.endsWith(".css")) return "text/css";
  else if(filename.endsWith(".js")) return "application/javascript";
  else if(filename.endsWith(".png")) return "image/png";
  else if(filename.endsWith(".gif")) return "image/gif";
  else if(filename.endsWith(".jpg")) return "image/jpeg";
  else if(filename.endsWith(".ico")) return "image/x-icon";
  else if(filename.endsWith(".xml")) return "text/xml";
  else if(filename.endsWith(".pdf")) return "application/x-pdf";
  else if(filename.endsWith(".zip")) return "application/x-zip";
  else if(filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}

bool handleStaticFile(String path) {
  if (path.endsWith("/")) path += "index.html";
  String contentType = getContentType(path);
  String pathWithGz = path + ".gz";
  if (SPIFFS.exists(pathWithGz)) path = pathWithGz;

  if (SPIFFS.exists(path)) {
    File file = SPIFFS.open(path, "r");
    size_t sent = webServer.streamFile(file, contentType);
    size_t contentLength = file.size();
    file.close();
    return true;
  }
  return false;
}

bool handleTemplatedFile(String templatePath, ProcessorCallback& keyProcessor) {
  ESPTemplateProcessor(webServer).send(templatePath, keyProcessor);
  return true;
}

void setup() {
  EEPROM.begin(EEPROM_CONFIG_LENGTH);

  SPIFFS.begin();
  Serial.begin(115200);

  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP("Beaus Capsule");

  // if DNSServer is started with "*" for domain name, it will reply with
  // provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", apIP);

  // replay to all requests with same HTML
  webServer.onNotFound([]() {
    handleStaticFile(String("/index.html"));
    //handleTemplatedFile(String("/index.html"), indexProcessor);
    //ESPTemplateProcessor(webServer).send(String("/index.html"), indexProcessor);
  });

  webServer.on("/colourPicker", [](){
     handleTemplatedFile(String("/colourPicker.html"), colourPickerProcessor);
     alert=false;
   });

  webServer.on("/alertOn", [](){
     webServer.send(200, "text/html", "on");
     alert=true;
   });

   webServer.on("/alertOff", [](){
      webServer.send(200, "text/html", "off");
      setColourFromPreset(0);
      alert=false;
    });

    webServer.on("/galaxy.jpg", [](){
      handleStaticFile("/galaxy.jpg");
    });

    webServer.on("/bootstrap.min.css", [](){
      handleStaticFile("/bootstrap.min.css");
    });

    webServer.on("/sliders.css", [](){
      handleStaticFile("/sliders.css");
    });

    webServer.on("/setColour", [](){
      uint8_t colourArray[4];
      String getColour = webServer.arg("colour");
      hexStringToByteArray(getColour, colourArray, 8);
      RgbwColor theColour = RgbwColor(colourArray[0], colourArray[1], colourArray[2], colourArray[3]);
      if(!topLightOnly){
        strip.ClearTo(theColour);
      }else{
        strip.ClearTo(black);
        strip.SetPixelColor(PixelCount-1, theColour );
      }
      strip.Show();
      webServer.send(200, "text/html", "set");

    });

    webServer.on("/storeColour", [](){
      uint8_t colourArray[4];
      String getColour = webServer.arg("colour");
      hexStringToByteArray(getColour, colourArray, 8);
      saveColour(colourArray[0], colourArray[1], colourArray[2], colourArray[3], 0);
      setColourFromPreset(0);
      webServer.send(200, "text/html", "saved");
    });

    webServer.on("/topOnly", [](){
      alert=false;
      topLightOnly=true;
      setColourFromPreset(0);
    });

    webServer.on("/cancelTopOnly", [](){
      alert=false;
      topLightOnly=false;
      setColourFromPreset(0);
    });

  webServer.begin();

  strip.Begin();
  strip.Show();
  setColourFromPreset(0);
}

void loop() {
  dnsServer.processNextRequest();
  webServer.handleClient();

  if(alert){
    currentColor = RgbwColor::LinearBlend(left, right, sweep);
    if(cleaningLeds){
      strip.SetPixelColor(ledNumber,black );
    }else{
      strip.SetPixelColor(ledNumber,currentColor );
    }
    strip.Show();
  }
  delay(100);
  if(sweepGoingUp){
    sweep = sweep+(1.0/PixelCount);
  }else{
    sweep = sweep-(1.0/PixelCount);
  }
  if(sweep>1.0){
    sweep=1.0;
    sweepGoingUp=false;
  }
  if(sweep<0.0){
    sweep=0.0;
    sweepGoingUp=true;
  }
  ledNumber = (ledNumber+1);
  if(ledNumber==PixelCount){
    ledNumber=0;
    cleaningLeds = !cleaningLeds;
  }
}
